from utils.utils import read_input_file, write_output_file

def main():
    input_filename = input()
    results_filename = 'results.txt'
    automaton, entries = read_input_file(input_filename)
    results = automaton.parse(entries)
    write_output_file(results_filename, results)

if __name__ == '__main__':
    main()