from abc import ABC, abstractmethod


def parse_transitions(raw_transitions: list):
    transitions = {}
    for transition in raw_transitions:
        t_list = transition.split()
        key = (int(t_list[0]), t_list[1])
        value = int(t_list[2])
        if key in transitions:
            transitions[key].append(value)
        else:
            transitions[key] = [value]
    return transitions


def is_deterministic(num_initial_states: int, transitions: dict):
    if num_initial_states > 1:
        return False

    return all(len(v) == 1 for v in transitions.values())


class Automaton(ABC):
    n_states: int
    term_symbols: tuple
    final_states: tuple
    transitions: dict
    REJECT: str = "rejeita"
    ACCEPT: str = "aceita"

    @abstractmethod
    def parse_entry(self, entry: str):
        pass

    @abstractmethod
    def parse(self, entries: tuple):
        pass

    # def __eq__(self, other):
    #     return


class AFD(Automaton):
    def __init__(self, n_states, term_symbols, final_states, transitions):
        self.n_states = n_states
        self.term_symbols = term_symbols
        self.final_states = final_states
        self.transitions = transitions
        self.initial_state = 0

    def _get_next_state(self, input):
        if input in self.transitions:
            return self.transitions[input][0]
        return None

    def parse_entry(self, entry: str):
        if entry == "-":
            if self.initial_state in self.final_states:
                return self.ACCEPT
            else:
                return self.REJECT

        cur_state = self.initial_state
        for ch in entry:
            transition_input = (cur_state, ch)
            next_state = self._get_next_state(transition_input)
            if next_state is None:
                return self.REJECT
            cur_state = next_state
        if cur_state in self.final_states:
            return self.ACCEPT
        return self.REJECT

    def parse(self, entries):
        results = []
        for entry in entries:
            results.append(self.parse_entry(entry))
        return results

    def __repr__(self):
        return f"AFD: states = {self.n_states}, terminal symbols = {self.term_symbols}, final states = {self.final_states}, transitions: {self.transitions}"

    def __eq__(self, other):
        return (
            isinstance(other, self.__class__)
            and other.n_states == self.n_states
            and other.term_symbols == self.term_symbols
            and other.final_states == self.final_states
            and other.transitions == self.transitions
        )


class AFN(Automaton):
    n_initial_states: int

    def __init__(
        self, n_states, term_symbols, n_initial_states, final_states, transitions
    ):
        self.n_states = n_states
        self.term_symbols = term_symbols
        self.n_initial_states = n_initial_states
        self.final_states = final_states
        self.transitions = transitions
        self.initial_states = list(range(n_initial_states))

    def _get_next_states(self, input):
        if input in self.transitions:
            next_states = self.transitions[input]
            return next_states
        return None

    def _parse_entry(self, cur_state, entry):
        if not entry or entry == "-":
            if cur_state in self.final_states:
                return [self.ACCEPT]
            return [self.REJECT]

        results = []
        for index, ch in enumerate(entry):
            transition_input = (cur_state, ch)
            next_states = self._get_next_states(transition_input)
            if next_states is None:
                return [self.REJECT]

            if len(next_states) > 1:
                mid_results = []
                for next_state in next_states:
                    mid_results += self._parse_entry(next_state, entry[index + 1 :])
                results += mid_results
                return results

            else:
                cur_state = next_states[0]
        if cur_state in self.final_states:
            results += [self.ACCEPT]
        else:
            results += [self.REJECT]
        return results

    def parse_entry(self, entry: str):
        results = []
        for initial_state in self.initial_states:
            results += self._parse_entry(initial_state, entry)
        if any([result == self.ACCEPT for result in results]):
            return self.ACCEPT
        return self.REJECT

    def parse(self, entries: tuple):
        results = []
        for entry in entries:
            results.append(self.parse_entry(entry))
        return results

    def __repr__(self):
        return f"AFN: states = {self.n_states}, terminal symbols = {self.term_symbols}, initial states = {list(range(self.n_initial_states))}, final states = {self.final_states}, transitions: {self.transitions}"

    def __eq__(self, other):
        return (
            isinstance(other, self.__class__)
            and other.n_states == self.n_states
            and other.term_symbols == self.term_symbols
            and other.n_initial_states == self.n_initial_states
            and other.final_states == self.final_states
            and other.transitions == self.transitions
        )
