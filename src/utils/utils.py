from model.automaton import AFD, AFN, is_deterministic, parse_transitions


def parse_term_symbols(line):
    inp_line = line.split()
    num = int(inp_line[0])
    items = []
    for i in range(num):
        items.append(inp_line[i + 1])
    return num, items


def parse_acc_states(line):
    inp_line = line.split()
    num = int(inp_line[0])
    items = []
    for i in range(num):
        items.append(int(inp_line[i + 1]))
    return num, items


def read_n_lines(num_lines, f):
    lines = [read_line(f) for i in range(num_lines)]
    return lines


def read_line(f):
    return f.readline().strip()


def read_input_file(filename):
    with open(filename, "r") as f:
        states = int(read_line(f))
        _, term_symbols = parse_term_symbols(read_line(f))
        num_initial_states = int(read_line(f))
        _, acc_states = parse_acc_states(read_line(f))
        num_transitions = int(read_line(f))
        raw_transitions = read_n_lines(num_transitions, f)
        num_entries = int(read_line(f))
        entries = read_n_lines(num_entries, f)

    transitions = parse_transitions(raw_transitions)
    if is_deterministic(num_initial_states, transitions):
        aut = AFD(states, term_symbols, acc_states, transitions)
    else:
        aut = AFN(states, term_symbols, num_initial_states, acc_states, transitions)
    return aut, entries

def read_output_file(filename):
    outputs = []
    with open(filename, 'r') as f:
        while (line := read_line(f)):
            outputs.append(line)
    return outputs

def write_output_file(filename, results):
    with open(filename, 'w') as f:
        for result in results:
            f.write(f'{result}\n')
